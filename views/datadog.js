(function(h,o,u,n,d) {
    h=h[d]=h[d]||{q:[],onReady:function(c){h.q.push(c)}}
    d=o.createElement(u);d.async=1;d.src=n
    n=o.getElementsByTagName(u)[0];n.parentNode.insertBefore(d,n)
})(window,document,'script','https://www.datadoghq-browser-agent.com/datadog-rum-v4.js','DD_RUM')
DD_RUM.onReady(function() {
    DD_RUM.init({
        clientToken: 'pub2d0b3ee8dfba90420e5925a691d241df',
        applicationId: '9e9b228e-0d40-475a-a991-1cbb7eb1db80',
        site: 'datadoghq.com',
        service:'auto-tts',
        // Specify a version number to identify the deployed version of your application in Datadog
        // version: '1.0.0',
        sampleRate: 100,
        replaySampleRate: 100,
        trackInteractions: true
    });
    DD_RUM.startSessionReplayRecording();
})

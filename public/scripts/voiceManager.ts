import Dexie from 'dexie';


class VoiceDatabase extends Dexie {
    voices!: Dexie.Table<Voice, string>;

    constructor() {
        super('voice');
        this.version(2).stores({
            voices: '++author, voiceName, pitch, rate'
        })
    }
}

interface Voice {
    author: string,
    voiceName: string,
    pitch: number,
    rate: number
}

const voiceDatabase = new VoiceDatabase();


export async function getVoice(author: string) {
    const voiceMap = new Map<string,SpeechSynthesisVoice>();

    for (const speechSynthesisVoice of window.speechSynthesis.getVoices().filter(v => v.lang.startsWith('en'))) {
        voiceMap.set(speechSynthesisVoice.name,speechSynthesisVoice);
    }
    let voice = await voiceDatabase.voices.get(author);
    if (!voice) {
        voice = {
            author,
            voiceName : Array.from(voiceMap.values())[Math.floor(Math.random() * voiceMap.size)].name,
            pitch: 1.3 - Math.random() * .6,
            rate: .7 + .6 * Math.random()
        };
        await voiceDatabase.voices.add({...voice, author});
    }
    return {voice:voiceMap.get(voice.voiceName),pitch: voice.pitch, rate: voice.rate}

}

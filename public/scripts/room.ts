let ws: WebSocket;
const chatMessagesElement = document.getElementById("chat-messages");
chatMessagesElement.scrollTop = chatMessagesElement.scrollHeight;
let lastMessageReceived: ChatMessageData;
import Hogan from 'hogan.js';
import {getVoice} from './voiceManager';

const messageTemplate = Hogan.compile(`
<p class="chat-message"><span class="message-timestamp">{{timestamp}}</span><b class="author">{{author}}: </b><span class="message-content">{{message}}</span></p>
`);

const notificationTemplate = Hogan.compile(`
<div class="notification is-{{color}} {{isLight}}">{{content}}</div>
`);

interface ChatMessage {
    type: 'message'
    data: ChatMessageData
}

interface ChatMessageData{
    author: string,
    message: string,
    created: number
}

interface ErrorMessage {
    type: 'error',
    data: string
}

interface NotificationMessage {
    type: 'notification',
    data: {
        content: string,
        color: string,
        isLight: string
    }
}

type Message = ChatMessage|ErrorMessage|NotificationMessage;

function connect() {
    console.log("connecting");
    const websocketProtocol = location.protocol === 'https:'?'wss':'ws';
    ws = new WebSocket(`${websocketProtocol}://${window.location.host}${window.location.pathname}/messages`);
    setTimeout(() => {
        ws.onmessage =  (event) => {
            const wsMessage: Message = JSON.parse(event.data);
            switch (wsMessage.type){
                case 'message':
                    handleChatMessage(wsMessage);
                    break;
                case 'error':
                    handleErrorMessage(wsMessage);
                    break;
                case 'notification':
                    handleNotificationMessage(wsMessage);
                    break;
            }
        };

    }, 100);
    ws.onerror = e => {
        console.error(e);
    };

    ws.onopen = () => {
        document.querySelectorAll('#chat-input div *').forEach(n => n.removeAttribute('disabled'));
    };

    ws.onclose = () => {
        document.querySelectorAll('#chat-input div *').forEach(n => n.setAttribute('disabled', ''));
        setTimeout(connect, 1000);
    };
}

function handleErrorMessage(errorMessage: ErrorMessage){
    console.error(`Websocket error: ${errorMessage.data}`)
    if(errorMessage.data==='Invalid session'){
        window.location.replace(`/login?to=${window.location.pathname}`)
    }
}

function handleChatMessage(chatMessage: ChatMessage){
    lastMessageReceived = chatMessage.data;
    const {author, message, created} = chatMessage.data;
    const render = messageTemplate.render({
        author,
        message,
        timestamp: new Date(created).toLocaleTimeString()
    });
    const element = document.createElement("template");
    element.innerHTML=render.trim();
    chatMessagesElement.append(element.content.firstChild);
    scrollToBottom();
    playMessage(chatMessage.data).then();
}

function handleNotificationMessage({data:{content,color,isLight}}: NotificationMessage){
    const render = notificationTemplate.render({content,color,isLight:isLight?'is-light':''});
    const element = document.createElement("template");
    element.innerHTML=render.trim();
    chatMessagesElement.append(element.content.firstChild);
    scrollToBottom();
}

function scrollToBottom(){
    if (chatMessagesElement.scrollHeight - chatMessagesElement.scrollTop - chatMessagesElement.clientHeight < 100)
        chatMessagesElement.scrollTop = chatMessagesElement.scrollHeight;
}

async function  playMessage(data: ChatMessageData) {
    const {author, message} = data;
    if ('speechSynthesis' in window) {
        const msg = new SpeechSynthesisUtterance();
        const voice = await getVoice(author);
        msg.text = message;
        msg.voice = voice.voice;
        msg.rate = voice.rate;
        msg.pitch = voice.pitch;
        window.speechSynthesis.speak(msg);
    }
}

connect();

export function sendMessage() {
    const elementById = document.getElementById("input-message") as any;
    const message = elementById.value;
    ws.send(JSON.stringify({message, author: "some user"}));
    elementById.value = '';
    return false;
}


document.getElementById("chat-messages").addEventListener('click', (e: any) => {
    for (const element of e.path as HTMLElement[]) {
        if (element.classList && element.classList.contains("chat-message")) {
            const data:any = {};
            for (const child of element.children){
               if(child.classList.contains("author")) {
                   data.author = child.textContent.trim();
                   data.author = data.author.slice(0,data.author.length-1)
               }
               if(child.classList.contains("message-content")){
                   data.message=child.textContent;
               }
            }
            playMessage(data).then();
        }
    }
});
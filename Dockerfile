FROM node:17
ENV NODE_ENV=production
RUN mkdir app
WORKDIR app
COPY package.json .
COPY dist ./dist
COPY views ./views
RUN npm i
ENTRYPOINT node dist/app.js
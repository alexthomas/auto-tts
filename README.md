#AUTO TTS
## About
This is just a project to develop familarity with eventstore and CQRS/Event sourcing
## Getting started
- You can run auto-tts by pulling down the `docker-compose.yml` and running `docker-compose up` this will pull the images for auto-tts and eventstore and run them
- You can include the following optional environment variables
    1. `PORT` - the port that the auto-tts server will run on, the default is `3000`
    2. `EVENTSTORE_HOST` - the eventstore server host/port, the default is `localhost:2113`
    3. `TWITCH_NICKNAME` - the nickname used to connect to twitch for chat cloning. This variable has no default and is required to enable twitch chat cloning.
    4. `TWITCH_PASSWORD` - the password that corresponds with the twitch nickname. If you need to generate a password [click here][1]
- You can develop locally by cloning the repository and running `npm run-script run`. Which will build the server typescript code, the frontend typescript code, and the sass.
    
    
[1]: https://twitchapps.com/tmi/

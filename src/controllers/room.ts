import {jsonEvent, StreamNotFoundError} from '@eventstore/db-client';
import {Request, Response} from 'express';
import {RoomCloneUpdatedEvent, RoomCreatedEvent, RoomNameUpdatedEvent} from '../events/room';
import {getEventStoreClient} from '../utils/connectors';
import {v4} from 'uuid';
import {logger} from '../utils/logger';


const client = getEventStoreClient();
const rooms = new Map<string, Room>();

interface CreateRoomRequest {
    name: string;
    twitchClone?: string,
}

interface Room {
    name,
    id,
    twitchClone,
    lastRevision
}


export async function createRoom(req: Request, res: Response) {
    const request: CreateRoomRequest = req.body;
    const id = v4();
    await saveRoom(id, request);
    while (!rooms.has(id)) {
        await new Promise(resolve => setTimeout(resolve, 20));
    }
    setTimeout(() => res.redirect(`/room/${id}`), 100);
}

async function saveRoom(id: string, request: CreateRoomRequest): Promise<any> {
    await client.appendToStream(`room/${id}`, jsonEvent<RoomCreatedEvent>({
        type: 'room-created',
        data: {id, ...request, twitchClone: request.twitchClone || undefined}
    }));
}

export function createRoomView(req: Request, res: Response) {
    if (!req.session.username) {
        res.redirect('/login?to=/room/create');
    } else {
        res.render('create-room', {hasTwitchConnection: !!process.env.TWITCH_NICKNAME});
    }
}

export async function getRoom(req: Request, res: Response) {
    const roomId = req.params.roomId;
    if (!req.session.username) {
        res.redirect(`/login?to=/room/${roomId}`);
    } else {
        const messages = [];
        try {
            for await (const {event} of client.readStream(`room/${roomId}/messages`, {
                direction: 'backwards',
                fromRevision: 'end',
                maxCount: 100
            }))
                messages.push({...event.data as object, created: new Date(event.created / 10000).toLocaleTimeString()});
        } catch (e) {
            if (e instanceof StreamNotFoundError)
                logger.debug(`${roomId} does not have any messages yet`);
            else
                logger.error(e);
        }
        messages.reverse();
        res.render('room', {title: rooms.get(roomId).name, messages, roomMissing: !rooms.has(roomId), id: roomId});
    }
}

export function getRooms(req: Request, res: Response) {
    res.render('rooms', {rooms: Array.from(rooms.values())});
}

export function editRoomView(req: Request, res: Response) {
    const roomId = req.params.roomId;
    res.render('edit-room', {room: rooms.get(roomId), hasTwitchConnection: !!process.env.TWITCH_NICKNAME});
}

export async function editRoom(req: Request, res: Response) {
    const request: CreateRoomRequest = req.body;
    const roomId = req.params.roomId;
    let nextExpectedRevision = rooms.get(roomId).lastRevision;
    if (request.name !== rooms.get(roomId).name) {
        const appendResult = await client.appendToStream(`room/${roomId}`, jsonEvent<RoomNameUpdatedEvent>({
            type: 'room-name-updated',
            data: {id: roomId, oldName: rooms.get(roomId).name, newName: request.name}
        }));
        nextExpectedRevision = appendResult.nextExpectedRevision;
    }
    if (request.twitchClone !== rooms.get(roomId).twitchClone) {
        const appendResult = await client.appendToStream(`room/${roomId}`, jsonEvent<RoomCloneUpdatedEvent>({
            type: 'room-clone-updated',
            data: {id: roomId, oldTwitchClone: rooms.get(roomId).twitchClone, newTwitchClone: request.twitchClone}
        }));
        nextExpectedRevision = appendResult.nextExpectedRevision;
    }
    let waitCount = 0;
    while (waitCount < 50 && nextExpectedRevision > rooms.get(roomId).lastRevision) {
        await new Promise((resolve) => setTimeout(resolve, 20));
        waitCount++;
    }
    res.redirect(`/room/${roomId}`);
}

async function registerRooms() {
    for await (const {event: {data, revision}} of client.subscribeToStream<RoomCreatedEvent>('$et-room-created', {resolveLinkTos: true})) {
        rooms.set(data.id, {...data, lastRevision: revision});
    }
}

async function registerRoomNameUpdates() {
    for await (const {event: {data, revision}} of client.subscribeToStream<RoomNameUpdatedEvent>('$et-room-name-updated', {resolveLinkTos: true})) {
        while (!rooms.has(data.id)) {
            await new Promise(resolve => setTimeout(resolve, 100));// sleep for 100 ms
        }
        rooms.get(data.id).name = data.newName;
        rooms.get(data.id).lastRevision = revision;
    }
}

async function registerRoomCloneUpdates() {
    for await (const {event: {data, revision}} of client.subscribeToStream<RoomCloneUpdatedEvent>('$et-room-clone-updated', {resolveLinkTos: true})) {
        while (!rooms.has(data.id)) {
            await new Promise(resolve => setTimeout(resolve, 100));// sleep for 100 ms
        }
        rooms.get(data.id).twitchClone = data.newTwitchClone;
        rooms.get(data.id).lastRevision = revision;
    }
}

registerRooms().then();
registerRoomNameUpdates().then();
registerRoomCloneUpdates().then();

import {jsonEvent, ResolvedEvent, StreamSubscription} from '@eventstore/db-client';
import WebSocket, {RawData} from 'ws';
import {ChatMessageEvent} from '../events/message';
import {RoomCommandEvent, RoomCreatedEvent, RoomNotificationEvent, UserEnteredEvent} from '../events/room';
import {getEventStoreClient} from '../utils/connectors';
import {logger} from '../utils/logger';


const connectionMap = new Map<string, WebSocket[]>();
const subscriptionMap = new Map<string, StreamSubscription<ChatMessageEvent>>();

const client = getEventStoreClient();

interface ChatMessage {
    author: string;
    message: string;
}


export function addClient(roomId: string, username: string, websocket: WebSocket, previouslyRetried?: boolean) {
    if (!connectionMap.has(roomId)) {
        if (previouslyRetried)
            websocket.send(JSON.stringify({type: 'error', data: 'Room not found'}));
        else
            setTimeout(() => addClient(roomId, username, websocket, true), 500);// give the server a chance to create the room
    } else if (!username) {
        websocket.send(JSON.stringify({type: 'error', data: 'Invalid session'}));
    } else {
        connectionMap.get(roomId).push(websocket);
        websocket.on('message', handleReceiveMessage.bind(this, roomId, username, websocket));
        client.appendToStream(`room/${roomId}`,jsonEvent<UserEnteredEvent>({
            type:'user-entered-room',
            data:{room:roomId,username,enterTime:new Date().getTime()}
        }))
    }
}

function handleReceiveMessage(roomId: string, username: string, websocket: WebSocket, message: RawData) {
    const chatMessage: ChatMessage = JSON.parse(message.toString());
    chatMessage.author = username;
    if (chatMessage.message.startsWith('!')) {
        handleChatCommand(roomId, username, chatMessage);
    } else {
        if (chatMessage.message.toLowerCase().indexOf('deez nuts') !== -1) {
            chatMessage.author = 'SYSTEM';
            chatMessage.message = 'You have been banned!';
        }
        client.appendToStream(`room/${roomId}/messages`, jsonEvent<ChatMessageEvent>({
            type: 'chat-message',
            data: {...chatMessage, room: roomId, sentTime: new Date().getTime()}
        })).then();
    }
}

function handleChatCommand(roomId: string, username: string, message: ChatMessage) {
    client.appendToStream(`/room/${roomId}`, jsonEvent<RoomCommandEvent>({
        type: 'room-command',
        data: {author: username, room: roomId, command: message.message, sentTime: new Date().getTime()}
    }))
}

function handleSendMessage(roomId: string, event: ResolvedEvent<ChatMessageEvent>) {
    const {event: {created, data: {author, message, room}}} = event;
    connectionMap.get(roomId).forEach(websocket => {
        websocket.send(JSON.stringify({type: 'message', data: {author, message, created: created / 10000}}));
    });
}

function handleSendNotification(event: RoomNotificationEvent) {
    const {content, isLight, color, room} = event.data;
    connectionMap.get(room).forEach(websocket => {
        websocket.send(JSON.stringify({type: 'notification', data: {content, isLight, color}}))
    })
}


async function registerRooms() {
    for await (const {event} of client.subscribeToStream<RoomCreatedEvent>('$et-room-created', {resolveLinkTos: true})) {
        const roomId = event.data.id;
        const streamingRead = client.subscribeToStream<ChatMessageEvent>(`room/${roomId}/messages`, {fromRevision: 'end'})
            .on('data', handleSendMessage.bind(this, roomId));
        subscriptionMap.set(roomId, streamingRead);
        connectionMap.set(roomId, []);
    }
}

async function registerRoomNotifications() {
    for await(const {event} of client.subscribeToStream<RoomNotificationEvent>('$et-room-notification', {
        resolveLinkTos: true,
        fromRevision: 'end'
    })) {
        handleSendNotification(event);
    }
}

registerRooms().then().catch(error=>logger.error(`Error registering rooms ${error}`));
registerRoomNotifications().then().catch(error=>logger.error(`Error registering room notifications ${error}`));
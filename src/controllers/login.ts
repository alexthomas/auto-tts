import {Request, Response} from 'express';
import {getEventStoreClient} from '../utils/connectors';
import {UserEnteredEvent} from '../events/room';

declare module 'express-session'{
    interface SessionData {
        username: string;
    }
}

const client = getEventStoreClient();
const existingUsernames = new Set<String>();

async function registerExistingUsernames(){

    for await (const {event} of client.subscribeToStream<UserEnteredEvent>("$et-user-entered-room",{resolveLinkTos:true})){
       existingUsernames.add(event.data.username)
    }
}

registerExistingUsernames().then();


export function login(req: Request, res: Response){
    const {username,to} = req.body;
    if(!username){
        res.render("login",{error:"A username is required"})
        return;
    }
    if(existingUsernames.has(username)){
        res.render("login",{error:"Username has already been used"})
        return;
    }
    req.session.username = username;
    req.session.save(()=>{
        if(to){
            res.redirect(to);
        } else{
            res.redirect("/");
        }
    });// save because we do a redirect right after the session won't exist by the time that the page is redirected
}

export function getLoginView(req: Request, res:Response){
    res.render("login",{to:req.query.to});
}
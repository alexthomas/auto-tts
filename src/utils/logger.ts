import {createLogger, format, transports} from 'winston';

export const logger = createLogger({
    format: format.combine(
        format.timestamp({}),
        format.splat(),
        format.colorize({all:true}),
        format.printf(info => `${info.timestamp} [${info.level}]: ${info.message}`)
    ),
    transports: new transports.Console()
});


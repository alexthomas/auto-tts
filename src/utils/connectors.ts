import {EventStoreDBClient} from '@eventstore/db-client';
import irc from 'irc';

const client = new EventStoreDBClient({
    endpoint: process.env.EVENTSTORE_HOST||"localhost:2113",
}, {insecure: true});

export function getEventStoreClient(){
    return client;
}


const ircClient = new irc.Client('irc.chat.twitch.tv', process.env.TWITCH_NICKNAME, {
    secure: true,
    port: 6697,
    password: process.env.TWITCH_PASSWORD,
    autoConnect: !!process.env.TWITCH_NICKNAME
});

export function getIrcClient(){
    return ircClient;
}

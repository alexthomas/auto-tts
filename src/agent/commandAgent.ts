import {jsonEvent } from "@eventstore/db-client";
import {
    persistentSubscriptionSettingsFromDefaults
} from '@eventstore/db-client/dist/utils/persistentSubscriptionSettings';
import {promises as fsPromise} from "fs";
import {RoomCommandEvent, RoomNotificationEvent} from "../events/room";
import {getEventStoreClient} from "../utils/connectors";
import {logger} from "../utils/logger";
import {ChatMessageEvent} from "../events/message";
import axios from "axios"

const client = getEventStoreClient();

function defaultCommand(room:String,command:String,author:String,relatedId:String){
    client.appendToStream(`room/${room}`, jsonEvent<RoomNotificationEvent>({
        type: "room-notification",
        data: {
            room,
            triggerEvent: relatedId,
            triggerTime: new Date().getTime(),
            content: `${author} executed: ${command?.slice(1)}`,
            color: "primary",
            isLight: false,
            readNotification: false
        },
        metadata: {relatedEvent: relatedId}
    })).then().catch(error=>logger.error(error));
}

async function splitBot(room:String,command:String,author:String,relatedId:String){
        const p1 = Math.floor(Math.random()*100);
        const p2 = Math.floor(Math.random()*100);
        return client.appendToStream(`room/${room}/messages`, jsonEvent<ChatMessageEvent>({
            type: "chat-message",
            data: {
                room,
                message:`Ok ${author} I"ll split it with you ${p1}/${p2}`,
                author:"SplitBot",
                sentTime: new Date().getTime()

            },
            metadata: {relatedEvent: relatedId}
        })).catch((error) =>logger.error(error));
}

async function redditPostBot(room:String,command:String,author:String,relatedId:String){
    let id = command.split(" ")[1];
    const redditUrlExpression = /https:\/\/www.reddit.com\/r\/\w+\/\w+\/([a-z0-9]+)\/\w+\/?/;
    if(redditUrlExpression.test(id)){
        id = redditUrlExpression.exec(id)[1];
    }
    if(/[a-z0-9]+/.test(id)){
    const axiosResponse = await axios.get(`https://api.reddit.com/api/info?id=t3_${id}`);
    if (axiosResponse.status<300) {
       const redditData = axiosResponse.data;
       const redditPost = redditData.data.children[0].data;
       const redditAuthor = redditPost.author;
       const redditBody = redditPost.selftext;
       const subreddit = redditPost.subreddit;
        return client.appendToStream(`room/${room}/messages`, jsonEvent<ChatMessageEvent>({
            type: "chat-message",
            data: {
                room,
                message:`@${author} from ${subreddit} - ${redditBody}`,
                author:redditAuthor,
                sentTime: new Date().getTime()

            },
            metadata: {relatedEvent: relatedId}
        })).catch((error) =>logger.error(error));

    }
    }
}

function printCommand(room, command, author, sentTime){
    return client.appendToStream(`room/${room}/messages`, jsonEvent<ChatMessageEvent>({
        type: "chat-message",
        data: {
            room,
            message:command,
            author,
            sentTime

        },
    })).then().catch(error=>logger.error(error));
}

/**
 * Executes the command submitted
 * @param room the room the command was executed in
 * @param command the content of the command
 * @param author the person who committed the command
 * @param relatedId the id of the command event
 * @returns boolean if the command should be displayed as a message
 */
function executeCommand(room:string,command:string,author:string,relatedId:string):[Function,boolean]{
    const commandKey = command.split(" ")[0].slice(1).toLowerCase();
    switch (commandKey) {
        case "splitit":
        case "split": return [splitBot.bind(this,room, command, author, relatedId), true];
        case "reddit": return [redditPostBot.bind(this,room,command,author,relatedId),true]
        default: return [defaultCommand.bind(this,room, command, author, relatedId), false];
    }
}

async function registerCommandEventSubscription() {
    logger.info("creating subscription")
    try{

    await client.createPersistentSubscription("$et-room-command","app-group",persistentSubscriptionSettingsFromDefaults({resolveLinkTos:true})).then()
    } catch (e){
        if(e.type!=="persistent-subscription-exists"){
            throw e;
        }
    }
    logger.info("connecting to subscription")
    const subscription = client.connectToPersistentSubscription<RoomCommandEvent>("$et-room-command","app-group");
    for await ( const resovledEvent  of subscription) {
        const {event, link} = resovledEvent;
        const {room, command, author, sentTime} = event.data;
        const [executeFunction,shouldPrint] = executeCommand(room,command,author,event.id);
        if(shouldPrint){
            await printCommand(room,command,author,sentTime);
        }
        executeFunction();
        await subscription.ack(resovledEvent)
    }
}

export function initialize() {
    registerCommandEventSubscription().then().catch(error=>logger.error("Error registering command events"+error));
}

import {AppendResult, EventData, jsonEvent} from '@eventstore/db-client';
import {ChatMessageEvent} from '../events/message';
import {RoomCloneUpdatedEvent, RoomCreatedEvent} from '../events/room';
import {getEventStoreClient, getIrcClient} from '../utils/connectors';
import {logger} from '../utils/logger';
import _ from 'lodash';

const client = getEventStoreClient();


const twitchMapping = new Map<string, string>();
let ircClient;

async function registerRooms() {
    for await (const {event} of client.subscribeToStream<RoomCreatedEvent>('$et-room-created', {resolveLinkTos: true})) {
        if (event.data.twitchClone) {
            setTimeout(() => {
                if(twitchMapping.has(event.data.twitchClone)){
                    ircClient.part(`#${event.data.twitchClone}`);
                    twitchMapping.delete(event.data.twitchClone);
                    logger.info('parting %s, it was already registered as a twitchMapping', event.data.twitchClone);
                }
                twitchMapping.set(event.data.twitchClone, event.data.id);
                logger.info('joining %s', event.data.twitchClone);
                ircClient.join(`#${event.data.twitchClone}`);
            }, 1000);
        }
    }
}

async function registerRoomCloneUpdates() {
    for await (const {event: {data}} of client.subscribeToStream<RoomCloneUpdatedEvent>('$et-room-clone-updated', {resolveLinkTos: true})) {
        if (data.oldTwitchClone) {
            let waitCounter = 0;
            while (!twitchMapping.has(data.oldTwitchClone) && waitCounter < 20) {
                await new Promise(resolve => setTimeout(resolve, 100));// sleep for 100 ms
                waitCounter++;
            }
            if (twitchMapping.has(data.oldTwitchClone)) {
                ircClient.part(`#${data.oldTwitchClone}`);
                twitchMapping.delete(data.oldTwitchClone);
                logger.info('parting %s', data.oldTwitchClone);
            }
        }
        if (data.newTwitchClone && twitchMapping.get(data.newTwitchClone) !== data.id) {
            setTimeout(() => {
                twitchMapping.set(data.newTwitchClone, data.id);
                logger.info('joining %s', data.newTwitchClone);
                ircClient.join(`#${data.newTwitchClone}`);
            }, 1000);
        }
    }
}


export function initialize() {
    const messages: Map<string, Array<EventData>> = new Map<string, Array<EventData>>();
    const saveMessages = _.debounce(()=>{
        for (let key of messages.keys()) {
            const roomMessages = messages.get(key);
            const saveStartTime = new Date();
            client.appendToStream(`room/${key}/messages`,roomMessages).catch(error=>logger.error("error saving messages" +error))
                .then(()=>{
                    logger.info(`saved ${roomMessages.length} messages for room ${key} in ${(new Date()).getTime()-saveStartTime.getTime()}ms`)
                })
            messages.delete(key)
        }
    },1000)
    if (process.env.TWITCH_NICKNAME) {
        ircClient = getIrcClient();
        ircClient.on('registered', () => {
            registerRooms().then();
            registerRoomCloneUpdates().then();
            ircClient.on('message#', (author, room, message) => {
                const roomId = twitchMapping.get(room.slice(1));
                messages.has(roomId) || messages.set(roomId, []);
                messages.get(roomId).push(
                    jsonEvent<ChatMessageEvent>({
                        type: 'chat-message',
                        data: {author, message, room: roomId, sentTime: new Date().getTime()}
                    }));
                saveMessages();
            });
            ircClient.on('error', message => {
                logger.error(message);
            });
        });
    }
}

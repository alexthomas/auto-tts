import {ChatMessageEvent} from './events/message';
import {getEventStoreClient} from './utils/connectors';
import {EventStoreDBClient, jsonEvent, JSONEventType} from '@eventstore/db-client';
import {v4} from 'uuid';
import {logger} from './utils/logger';

const oldClient = getEventStoreClient();
const newClient = new EventStoreDBClient({
    endpoint: "localhost:2213",
}, {insecure: true});

async function getRoomNames(): Promise<Set<string>> {
    const events = oldClient.readStream("$et-room-created", {resolveLinkTos: true});
    const names = new Set<string>();
    for await (const {event} of events) {
        logger.debug(event);
        //@ts-ignore
        names.add(event.data.roomName);
    }
    return names;
}

type NewRoomCreatedEvent = JSONEventType<'room-created', {
    name,
    id,
    twitchClone
}>
type NewChatMessageEvent = JSONEventType<'chat-message', { room, message, author, sentTime }>


async function saveMessages(idMappings:Map<string,string>){
    const events = oldClient.subscribeToStream<ChatMessageEvent>("$et-chat-message", {resolveLinkTos:true});
    for await (const {event:{data,created}} of events){
        const roomId = idMappings.get(data.room)
        await newClient.appendToStream(`room/${roomId}/message`, jsonEvent<NewChatMessageEvent>({
            type: 'chat-message', data: {
                ...data,
                room: roomId,
                sentTime: created/10000
            }
        }))
    }
}

getRoomNames().then(names => {
    const idMappings = new Map<string, string>();
    for (let name of names) {
        const id = v4();
        idMappings.set(name, id);
        const twitchClone = name.startsWith('clone-')?name.slice(6):undefined;
        newClient.appendToStream(`room/${id}`, jsonEvent<NewRoomCreatedEvent>({
            type: 'room-created',
            data: {name, id,twitchClone}
        }));
    }
    return saveMessages(idMappings);
}).then();
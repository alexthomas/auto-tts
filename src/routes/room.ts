import express from 'express';
import {createRoom, createRoomView, editRoom, editRoomView, getRoom, getRooms} from '../controllers/room';


const router = express.Router();
router.post('/create', createRoom);
router.get('/create', createRoomView);
router.get('/', getRooms);
router.get('/:roomId', getRoom);
router.get('/:roomId/edit', editRoomView);
router.post('/:roomId/edit', editRoom);
export {router};

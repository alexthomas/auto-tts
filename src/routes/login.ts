import express from 'express';
import {login, getLoginView} from '../controllers/login';

const router = express.Router();
router.post('/',login);
router.get('/',getLoginView);

export {router};
import express from "express";
import bodyParser from 'body-parser';
import {router as roomRouter} from './routes/room';
import {router as loginRouter} from './routes/login';
import * as path from 'path';
import {addClient} from './controllers/chat';
import session from 'express-session'
import ExpressWebsocket from 'express-ws';
import FileStore from 'session-file-store';
import {initialize as twitchInitialize} from './agent/twitchAgent';
import {initialize as commandInitialize} from './agent/commandAgent';

const app = express();
const expressWs = ExpressWebsocket(app); // enables websockets
const port = process.env.PORT || 3000;
app.set('view engine', 'pug');
app.use('/assets', express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({extended: true}));
app.use(session({
    secret: 'static secret',
    resave: false,
    store: new (FileStore(session))({})
}))

app.get('/', (req, res) => {
    res.render("index");
});
app.use('/room', roomRouter);
app.use('/login', loginRouter);

// @ts-ignore
app.ws('/room/:roomId/messages', (ws,req)=>{
    addClient(req.params.roomId,req.session.username,ws);
})
app.listen(port);

twitchInitialize();
commandInitialize();

import {JSONEventType} from '@eventstore/db-client';

export type ChatMessageEvent = JSONEventType<'chat-message', { room, message, author, sentTime }>

export type MessageEvent = ChatMessageEvent;
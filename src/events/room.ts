import {JSONEventType} from '@eventstore/db-client';


export type RoomCreatedEvent = JSONEventType<'room-created', {
    name,
    id,
    twitchClone
}>

export type RoomNameUpdatedEvent = JSONEventType<'room-name-updated', {
    oldName,
    newName,
    id
}>

export type RoomCloneUpdatedEvent = JSONEventType<'room-clone-updated', {
    oldTwitchClone,
    newTwitchClone,
    id
}>


export type RoomCommandEvent = JSONEventType<'room-command', { room, command, author, sentTime }>

export type RoomNotificationEvent = JSONEventType<'room-notification', { room, triggerEvent, triggerTime, content, color, isLight, readNotification }>

export type UserEnteredEvent = JSONEventType<'user-entered-room', {room, username,enterTime}>

export type RoomEvent = RoomCreatedEvent | RoomNameUpdatedEvent | RoomCloneUpdatedEvent | RoomCommandEvent | RoomNotificationEvent | UserEnteredEvent;
